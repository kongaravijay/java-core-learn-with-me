package io.learn;

import java.io.FileNotFoundException;
import java.io.IOException;

public abstract class Child extends Parent {

    public Number add(int a, int b) throws FileNotFoundException {
        return (double)(a+b);
    }

    public static String sample() {
        return "Child sample";
    }
}
