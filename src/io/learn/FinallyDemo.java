package io.learn;

public class FinallyDemo {
    public static void main(String[] args) throws Throwable {
        FinallyDemo demo = new FinallyDemo();
        demo.test();
        demo.finalize();
        demo = null;
        System.gc();
        Thread.sleep(2000);
    }

    private void test() {
        try {
            System.out.println("try block");
            System.out.println(10/0);
        } catch (Exception e) {
            System.out.println("catch block");
        } finally {
            System.out.println("finally block");
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("finalize method called");
    }
}
