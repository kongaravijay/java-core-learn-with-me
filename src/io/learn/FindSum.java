package io.learn;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This calss is used to finding sum
 */
 public class FindSum {

    public static void main (String[] args){

        Scanner scanner = new Scanner(System.in);


        try {
            //int a = Integer.parseInt(args[0]);
            //int b = Integer.parseInt(args[1]);

           /* System.out.print("enter an integer: ");
            int a = scanner.nextInt();*/
            System.out.println("enter length: ");
            int ln = scanner.nextInt();
            int[] array = new int[ln];


            int i = 0;
            System.out.println("enter integer: ");
            while(scanner.hasNextInt()){
                System.out.println("enter integer: ");
                array[i] = scanner.nextInt();
                i++;
                if(i == ln){
                    break;
                }
            }


            int[] inp = new int[args.length];


            for(int j= 0; j< args.length; j++){
                inp[j] = Integer.parseInt(args[j]);
            }

            FindSum findSum = new FindSum();
            System.out.println("sum of given integers is: "+ findSum.sum(array));

        } catch (ArrayIndexOutOfBoundsException ae){
            System.out.println("Please provide valid input");
        } catch (InputMismatchException im){
            System.out.println("Please provide valid integer");
        } catch (NumberFormatException ne){
            System.out.println("Please provide valid integer");
        }

    }

    private  int sum(int a, int b){

        return a+b;
    }



    private  int sum( int[] a){

        if(a.length == 0)
            throw new RuntimeException("provide atleast two integers");

        int sum = 0;
        for (int i =0; i < a.length; i++){

            sum = sum + a[i];

        }

        return sum;
    }


}
