package io.learn;

public class OverloadingDemo {
    public static void main(String[] args) {
        OverloadingDemo demo = new OverloadingDemo();
        demo.add(10, 20); //int 2 arg
        //demo.add(10.5f, 20.5f); //float 2 arg
        //demo.add(10.5, 20.5); //double 2 arg
        //demo.add(10, 20, 30); //int 3 arg
        demo.add('a', 'b'); //int 2 arg
        //demo.add(10, 20.5f);//float 2 arg
        //demo.add(10.5f, 20);//float 2 arg
        demo.add(10, 'b'); //exception
        demo.add('a', 10.5); //exception
        //demo.add(10.5f, 20.5);//float 2 arg
    }

    private void add(int a, int b) {
        System.out.println("SUm of 2 integers is : " + (a+b));
    }

    private void add(float a, float b) {
        System.out.println("SUm of 2 floats is : " + (a+b));
    }

    private void add(int a, double b) {
        System.out.println("SUm of 2 double is : " + (a+b));
    }

    private void add(double a, int b) {
        System.out.println("SUm of 2 double is : " + (a+b));
    }

    /*private void add(int a, int b, int c) {
        System.out.println("SUm of 3 integers is : " + (a+b+c));
    }*/

}
