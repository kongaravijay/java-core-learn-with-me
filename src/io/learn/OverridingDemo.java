package io.learn;

public class OverridingDemo {

    public static void main(String[] args) {
        Parent p = new Parent();
        System.out.println(p.sample()); //parent method

        Child c = new GrandChild();
        System.out.println(c.sample());//grandchild

        Parent  p1 = new GrandChild();
        System.out.println(p1.sample());//grandchild
    }
}
