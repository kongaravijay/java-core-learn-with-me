package io.learn;

import java.io.IOException;

public class Parent {

    public strictfp Number add(int a, int b) throws IOException {
        return (a+b);
    }

    public static String sample() {
        return "Parent sample method";
    }
}
