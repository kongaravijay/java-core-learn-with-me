package io.learn.exception;

public class CustomExceptionDemo {
    public static void main(String[] args) {
        double withdrawAmount = 5000;
        double balance = 1000;
        if (withdrawAmount > balance) {
            throw new InsufficientFundsException("You do not have sufficient funds in your account");
        }
    }

}
