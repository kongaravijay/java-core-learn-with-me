package io.learn.exception;

public class ExceptionFlow {

    public static void main(String[] args) {
        try {
            System.out.println("Statement0");//1
            try {
                System.out.println("Statement1");
                System.out.println("Statement2" + (10 / 0));
            } catch (ArithmeticException ae) {
                System.out.println("Statement3");
                try {
                    System.out.println("Statement4");
                    System.out.println("Statement5" + Integer.parseInt("hello"));
                } catch (NullPointerException npe) {
                    System.out.println("Statement6");
                } finally {
                    System.out.println("Statement7");
                }
            } catch (Exception e) {
                System.out.println("Statement8");
            } finally {
                System.out.println("Statement9");
            }
        } catch (Exception e1) {
            System.out.println("Statement10");
            throw new RuntimeException("Runtime exception");
        } finally {
            System.out.println("Statement11");
        }
    }
}
