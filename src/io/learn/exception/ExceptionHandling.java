package io.learn.exception;

public class ExceptionHandling {
    public static void main(String[] args) {
        System.out.println("Main method started"); //1
        m1();
        System.out.println("Main method ended");//7
    }

    private static void m1() {
        System.out.println("m1 method started");//2
        try {
            m2();
        } catch (Exception e) {
            System.out.println("Exception occurred in m2");//5
        }
        System.out.println("m1 method ended");//6
    }

    private static void m2() {
        System.out.println("m2 method started");//3
        m3();
        System.out.println("m2 method ended");
    }

    private static void m3() {
        System.out.println("m3 method started");//4
        System.out.println("Result is:" + (100/0));//ArithmeticException
        System.out.println("m3 method ended");
    }
}
