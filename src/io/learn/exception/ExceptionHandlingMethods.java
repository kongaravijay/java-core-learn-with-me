package io.learn.exception;

public class ExceptionHandlingMethods {

    public static void main(String[] args) {
        try {
            System.out.println(Integer.parseInt("hello"));
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage()); // description
            System.out.println(e); // Exception name : description
            e.printStackTrace(); // logging purpose
        }
    }
}
