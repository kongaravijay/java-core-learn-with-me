package io.sesgo.collections;

import java.util.*;

class StringLengthComparator implements Comparator<String> {
	@Override
	public int compare(String value1, String value2) {
		return Integer.compare(value2.length(),
				value1.length());
	}

}

public class QueueRunner {

	public static void main(String[] args) {
		Queue<String> queue = new PriorityQueue<>(10,
				new StringLengthComparator());
     List<String> stringList = new ArrayList<>();
        stringList.add("Zebra");
        stringList.add("Monkey");
        stringList.add("Cat");
        stringList.add("A");
        stringList.add("B");
        stringList.add("C");
        stringList.add("D");
        stringList.add("E");
        stringList.add("F");
        stringList.add("G");
		queue.addAll(stringList);
		queue.offer("Z");
		while (queue.peek() != null)
			System.out.println(queue.poll());
	}

}
