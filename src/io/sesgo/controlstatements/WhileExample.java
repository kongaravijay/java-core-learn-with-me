package io.sesgo.controlstatements;

public class WhileExample {
    public static void main(String[] args) {

        //WHILE loop
        int i = 1;
        while (i <= 6) {
            System.out.println("i value is " + i);
            i++;
        }
        //FOR loop
        for (int j = 1; j < 5; j++) {
            System.out.println("j value is " + j);
        }

        //Do While
        do {
            System.out.println(i);
            i++;
        } while (i <= 10);
    }
}
