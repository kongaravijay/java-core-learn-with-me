package io.sesgo.exception;

public class ExceptionDemo {

    /**
     * Throwable-> Error, Exception
     * Exception-> Checked, Unchecked
     * Checked (Compile time)-> IOException, SQLException
     * unchecked(Run time)-> Exception
     */

    public static void main(String[] args) {
        //Unchecked
        int i, j, k;
        i = 10;
        j = 0;
        k = 0;
        try {
            k = i / j;
        } catch (Exception ex) {
            System.out.println("Exception occurred " + ex);
        }
    }
}
