package io.sesgo.functionalprogramming;

import java.util.ArrayList;
import java.util.List;

public class MethodReferencesRunner {

public static void print(Integer number) {
		System.out.println(number);
	}

	public static void main(String[] args) {
    List<String> stringList = new ArrayList<>();
        stringList.add("Ant");
        stringList.add("Bat");
        stringList.add("Dog");
        stringList.add("Elephant");

        List<Integer> integerList = new ArrayList<>();
        integerList.add(23);
        integerList.add(45);
        integerList.add(67);
        integerList.add(34);

        stringList.stream().map(s -> s.length())
				.forEach(s -> MethodReferencesRunner
						.print(s));

        stringList.stream().map(String::length)
				.forEach(MethodReferencesRunner::print);

        /*integerList.filter(n -> n%2==0)
				.max( (n1,n2) -> Integer.compare(n1,n2) )
				.orElse(0);*/
		
		Integer max = integerList.stream()
				.filter(MethodReferencesRunner::isEven)
				.max(Integer::compare)
				.orElse(0);
		
		System.out.println(max);
	}

	public static boolean isEven(Integer number) {
		return number % 2 == 0;
	}


}
