package io.sesgo.oops.interfaces;

public interface ComplexAlgorithm {
	int complexAlgorithm(int number1, int number2);
}
